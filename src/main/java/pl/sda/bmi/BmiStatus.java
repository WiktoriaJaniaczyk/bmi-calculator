package pl.sda.bmi;

public enum BmiStatus {
NIE("NIEDOWAGA"),
    OPT("OPTIMUM"),
    NAD("NADWAGA"),
    OTY("OTYLOSC");

private String description;

    BmiStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

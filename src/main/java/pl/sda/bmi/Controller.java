package pl.sda.bmi;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class Controller {


    @FXML
    private JFXTextField weight;

    @FXML
    private JFXTextField height;

    @FXML
    private JFXButton calculate;

    @FXML
    private JFXTextField bmiField;

    @FXML
    private JFXTextField statusField;

    //2 sposob - definicja eventu w scene bilderze -> code -> on action
    @FXML
    void calculateBmi(ActionEvent event) {

        double weight = Double.parseDouble(this.weight.getText());
        double height = Double.parseDouble(this.height.getText());

        double bmiIndex = calculateBMI(weight, height);
        bmiField.setText(String.valueOf(bmiIndex));
        calculateStatus(bmiIndex);

    }

    private double calculateBMI (double weight, double height){
//        double bmiCalc = Math.round((weight * 100 * 100) / (height * height));

        return Math.round((weight * 100 * 100) / (height * height));

    }


    // jeden sposob - kompilator szuka metody initialize jesli ja znajdzie - uruchamia - i trwa ona az do wylaczenia aplikacji
//    public void initialize(){
////        System.out.println("Halo");
//        calculate.setOnAction(actionEvent -> {
//            System.out.println("klikasz!");
//        });
//    }


    private void calculateStatus(double bmiValue) {

//        //na podstawie bmi zwroc status
        if (bmiValue < 18.5) {
            statusField.setText(String.valueOf(BmiStatus.NIE.getDescription()));

        } else if (bmiValue <= 24.99) {
            statusField.setText(String.valueOf(BmiStatus.OPT.getDescription()));

        }else if(bmiValue < 30){
            statusField.setText(String.valueOf(BmiStatus.NAD.getDescription()));

        }else
            statusField.setText(String.valueOf(BmiStatus.OTY.getDescription()));
//        statusField.setText("Your bmi is ok!");


    }


}